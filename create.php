<?php
require('includes/functions.php');
check_login();
$tasks	=	get_tasks($conn,10);
if(isset($_POST['task_name'])){
    
    $data           =   $_POST;
    $data['conn']   =   $conn;
    $add_task = add_new_task($data);
}

include_once('includes/header.php');
?>

            <div class="row">
                <h1>Welcome <?php echo current_user('name');?>!</h1>
                <hr>

                <div class="col-lg-4">
                    <h2><i class="fa fa-plus-circle"></i> Add New task</h2>
                     <?php
                    read_alert();
                    ?> 
                	<form class="form" action="" method="post">
                        <div class="form-group">
                            <label>Task Name</label>
                            <textarea class="form-control" name="task_name" placeholder="Enter task name" required></textarea>
                        </div>

                        <div class="form-group">
                            <label>Task Priority</label>
                            <select class="form-control" name="priority" required>
                                <option selected="" value="1">Priority 1</option>
                                <option  value="2">Priority 2</option>
                                <option  value="3">Priority 3</option>
                                <option  value="4">Priority 4</option>
                                <option  value="5">Priority 5</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary"><i class="fa fa-save fa-lg"></i> Save Task</button>
                        </div>

                    </form>


                </div>
            </div>
            <!-- /.row -->
<?php
include_once('includes/footer.php');