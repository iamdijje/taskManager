<?php
require('database.php');
$conn = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);

function set_flashdata($name,$value){
	if(!$name OR !$value){
		return false;
	}

	$_SESSION['flash_'.$name]	=	$value;
	return true;
}


function read_flash($name){
	if(!isset($_SESSION['flash_'.$name])){
		return false;
	}
	$value 	=	$_SESSION['flash_'.$name];
	if($value==NULL OR $value=='' OR $name=='' OR $name==NULL){
		return false;
	}

	if($value=='' OR $value==NULL){
		return false;
	}
	unset($_SESSION['flash_'.$name]);
	return $value;
}


function redirect_msg($msg,$type,$redirect){
	if(!$msg OR !$type OR !$redirect){
		return false;
	}

	set_flashdata('msg',$msg);
	set_flashdata('type',$type);
	header('location:'.$redirect);
	exit();


}

function read_alert(){
	$msg 	=	read_flash('msg');
	$type 	=	read_flash('type');
	if($msg=='' OR $msg==NULL OR $type==NULL OR $type==''){
		return false;
	}
	?>
	<div class="alert alert-<?php echo $type?>">
		<p><?php echo $msg?></p>
	</div>
	<?php
}

function check_login(){
	$user_info	=	$_SESSION['user_id'];
	if(!$user_info){
		redirect_msg('You need to login first','danger','login.php');
	}
} 

function register_user($data){

	if($data['name']=='' OR $data['email']=='' OR $data['password']=='' OR $data['confirm']==''){
		redirect_msg('Please fill out all fields','danger','register.php');
	}

	if($data['password']!=$data['confirm']){
		redirect_msg('Password and confirm password should be same','danger','register.php');
	}

	if(strlen($data['password'])<6){
		redirect_msg('Password length should be at least 6 character long','danger','register.php');
	}
	$name 	=	$data['name'];
	$email	=	$data['email'];
	$password=	sha1($data['password']);

	$sql = "SELECT * FROM `user` WHERE `email`='{$email}'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		redirect_msg('This email is already registerd, please try another','danger','register.php');
	}


	$sql 	=	"INSERT INTO user (name,email,password,status) VALUES ('{$name}','{$email}','{$password}',1)";
	
	if(mysqli_query($data['conn'],$sql)==TRUE){
		redirect_msg('Your account created, please login to continue','success','login.php');
	}else{
		redirect_msg('Error in creating account, please try again'.$mysqli_error($conn),'danger','register.php');
	}	
}


function login_process($data){
	if($data['email']=='' OR $data['password']=='' OR $data['conn']==''){

		redirect_msg('Please provide email and password','danger','login.php');
	}
	$password 	=	sha1($data['password']);
	$email 		=	$data['email'];
	$conn 		=	$data['conn'];
	$sql = "SELECT * FROM `user` WHERE `email`='{$email}' AND `password` = '{$password}'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		$_SESSION['user_id']	=	$row['id'];
		$_SESSION['name']		=	$row['name'];
		$_SESSION['email']		=	$row['email'];

		header('location:index.php');
		exit();

	}else{
		redirect_msg('Incorrect Email and password','danger','login.php');
	}

}

function current_user($name){
	if($_SESSION[$name]!=''){
		return $_SESSION[$name];
	}
}


function display_msg($msg,$type,$icon='info-circle'){
	?>
	<div class="alert alert-<?php echo $type?>">
		<p><i class="fa fa-<?php echo $icon;?>"></i> <?php echo $msg?></p>
	</div>
	<?php
}

function get_tasks($conn,$num=100){
	$user_id 	=	current_user('user_id');
	$sql = "SELECT * FROM `task` WHERE `user_id`='{$user_id}' AND `task_done`= '0' LIMIT $num ";
	
	$result = $conn->query($sql);
	if(!$result){
		echo mysqli_error($conn);
	}
	if($result->num_rows>0){
		return $result;
	}else{
		return false;
	}
}

function add_new_task($data){
	$task_name		=	$data['task_name'];
	$task_priority	=	$data['priority'];
	$task_user_id	=	current_user('user_id');
	$conn 			=	$data['conn'];

	$sql 	=	"INSERT INTO task (task_name,priority,user_id) VALUES ('{$task_name}','{$task_priority}','{$task_user_id}')";
	$result = $conn->query($sql);
	if(mysqli_query($data['conn'],$sql)){
		redirect_msg('New Task added successfully','success','create.php');
	}else{
		redirect_msg('Error in adding new task, please try again soon'.mysqli_error($conn),'danger','create.php');
	}	
}
