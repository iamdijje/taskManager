<?php 
require('includes/functions.php');
if(isset($_POST['email'])){
	$data			=	$_POST;
	$data['conn']	=	$conn;
	login_process($data);
	exit();
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Task Manager Login</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/login.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
</head>
<body>
	<div class = "container">
	<div class="wrapper">
		<form action="" method="post"  name="Login_Form" class="form-signin">
			      
		    <h3 class="form-signin-heading">Welcome Back! Please Sign In</h3>
		    <p class="text-center"><img src="assets/images/logo.png" alt="LOGO" width="100px" align="center"></p>
		    <?php
			read_alert();
			?> 
			  <hr class="colorgraph"><br>
			  <a href="index.php"><h4 class="text-center"><i class="fa fa-home"></i> Home</h4></a>
			  <input type="email" class="form-control" name="email" placeholder="Your email" required="" autofocus="" />
			  <input type="password" class="form-control" name="password" placeholder="Password" required=""/>     		  
			 
			  <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Login</button>
			  <p class="text-center"><a href="register.php">Register</a></p>  			
		</form>			
	</div>
</div>
</body>
</html>