<?php
require('includes/functions.php');
	if(isset($_POST['name'])){
		$post_data	=	$_POST;
		$post_data['conn']=	$conn;
		
		register_user($post_data);
	}
?>
<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

		<!-- Website CSS style -->
		<link rel="stylesheet" type="text/css" href="assets/css/register.css">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Admin</title>
	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Register : Task Manager</h1>
	               		<p class="text-center"><img src="assets/images/logo.png" alt="LOGO" width="100px" align="center"></p>
	               		<hr />
	               		 
	               	</div>
	            </div> 
				<div class="main-login main-center">
					<?php
							read_alert();
							?> 
					<form class="form-horizontal" method="post" action="">
						
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Your Name</label>
							<input type="text" class="form-control" name="name" placeholder="Enter your Name" required/>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<input type="text" class="form-control" name="email"  placeholder="Enter your Email" required/>
						</div>

						

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<input type="password" class="form-control" name="password"  placeholder="Enter your Password" required/>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
							<input type="password" class="form-control" name="confirm" placeholder="Confirm your Password" required/>
						</div>

						<div class="form-group ">
							<button class="btn btn-primary btn-lg btn-block">Register</button>
						</div>
						<div class="login-register">
				            <a href="login.php">Login</a>
				         </div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>