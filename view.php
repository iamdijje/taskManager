<?php
require('includes/functions.php');
check_login();
$tasks	=	get_tasks($conn,100);

include_once('includes/header.php');
?>

            <div class="row" id="main" >
                <h1>Welcome <?php echo current_user('name');?>!</h1>
                <hr>

                <div class="panel panel-default col-lg-12">
                	<h3>Current tasks</h3>
                	<?php
                	if($tasks):
                	?>
                	<table class="table table-hover">
                		<thead>
                			<tr>
                				<th>Sr</th>
                				<th>Task Name</th>
                				<th>Task Priority</th>
                				<th>Task Created</th>
                				<th>Task Staus</th>
                				<th>Action</th>
                			</tr>
                		</thead>

                		<tbody>
                			<?php
                			while($row=$tasks->fetch_assoc()){
                				?>
                				<tr>
                					<td>#</td>
                					<td><?php echo $row['task_name']?></td>
                					<td><?php echo $row['priority'];?></td>
                					<td><?php echo date(DATE_TIME_FORMAT,strtotime($row['created_at']))?></td>
                					<td><?php if($row['task_done']==1){ echo "<i class='fa-fw fa-lg fa fa-check-circle text-success'></i>";}else{
                						echo "<i class='fa-fw fa-lg fa fa-clock-o text-warning'></i>";
                					}

                					?></td>
                					<td>
                						<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                						<button class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></button>
                						<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                					</td>
                				</tr>
                				<?php
                			}
                			?>
                		</tbody>

                	</table>

                	<?php
                	else:
                		display_msg('No Task added yet','info');

                	endif;
                	?>


                </div>
            </div>
            <!-- /.row -->
<?php
include_once('includes/footer.php');